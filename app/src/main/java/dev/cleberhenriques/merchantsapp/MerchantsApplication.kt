package dev.cleberhenriques.merchantsapp

import android.app.Application
import dev.cleberhenriques.core.di.CoreComponent
import dev.cleberhenriques.core.di.CoreComponentProvider
import dev.cleberhenriques.core.di.DaggerCoreComponent
import dev.cleberhenriques.core.di.NetworkModule


class MerchantsApplication: Application(), CoreComponentProvider {

    override val coreComponent: CoreComponent
        get() = DaggerCoreComponent.factory().create(NetworkModule("https://api.quandoo.com"))

    override fun onCreate() {
        super.onCreate()
    }
}