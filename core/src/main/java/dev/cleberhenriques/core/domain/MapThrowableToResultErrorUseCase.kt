package dev.cleberhenriques.core.domain

import com.google.gson.Gson
import dev.cleberhenriques.core.data.api.ErrorResponse
import retrofit2.HttpException
import java.io.InterruptedIOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class MapThrowableToResultErrorUseCase {

    companion object {
        private val UNEXPECTED_ERROR = "UNEXPECTED_ERROR"
        private val TIMEOUT_ERROR = "TIMEOUT_ERROR"
        private val CONNECTIVITY_ERROR = "CONNECTIVITY_ERROR"

        val defaultError = Result.Error(
            UNEXPECTED_ERROR,
            "Sorry, something went wrong"
        )
        val timeOutError = Result.Error(
            TIMEOUT_ERROR,
            "Connection time to server timed out, please check your internet connection."
        )

        val connectivityError = Result.Error(
            CONNECTIVITY_ERROR,
            "Unable to connect to server, please check your internet connection."
        )
    }

    operator fun invoke(exception: Throwable): Result.Error {

        return when (exception) {
            is SocketTimeoutException -> timeOutError
            is InterruptedIOException -> timeOutError
            is UnknownHostException -> connectivityError
            is HttpException -> {
                val body = exception.response()?.errorBody()?.string()
                try {
                    val errorResponse = Gson().fromJson(body, ErrorResponse::class.java)
                    if (errorResponse.type.isNullOrEmpty() || errorResponse.message.isNullOrEmpty()) {
                        defaultError
                    } else {
                        Result.Error(errorResponse.type, errorResponse.message, exception)
                    }
                } catch (ex: java.lang.Exception) {
                    defaultError
                }
            }
            else -> defaultError
        }.copy(exception = exception)
    }

}