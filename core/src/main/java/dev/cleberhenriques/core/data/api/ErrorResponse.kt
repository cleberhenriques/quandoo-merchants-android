package dev.cleberhenriques.core.data.api

import com.google.gson.annotations.SerializedName

data class ErrorResponse (
    @SerializedName("errorType")
    val type: String?,
    @SerializedName("errorMessage")
    val message: String?
)