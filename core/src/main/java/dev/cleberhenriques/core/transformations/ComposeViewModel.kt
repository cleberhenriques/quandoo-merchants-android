package dev.cleberhenriques.core.transformations

import dev.cleberhenriques.core.domain.Result
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class ComposeViewModel<T> : SingleTransformer<T, Result<T>>,
    ObservableTransformer<T, Result<T>> {

    override fun apply(upstream: Single<T>): SingleSource<Result<T>> {
        return upstream
            .compose(ComposeResult())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun apply(upstream: Observable<T>): ObservableSource<Result<T>> {
        return upstream
            .compose(ComposeResult())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
