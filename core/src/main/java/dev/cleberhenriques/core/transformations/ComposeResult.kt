package dev.cleberhenriques.core.transformations

import dev.cleberhenriques.core.domain.MapThrowableToResultErrorUseCase
import dev.cleberhenriques.core.domain.Result
import io.reactivex.*

open class ComposeResult<T> : SingleTransformer<T, Result<T>>,
    ObservableTransformer<T, Result<T>> {

    val mapThrowableToResultErrorUseCase =
        MapThrowableToResultErrorUseCase()

    override fun apply(upstream: Single<T>): SingleSource<Result<T>> {
        return upstream
            .map { Result.Success(it) as Result<T> }
            .onErrorReturn { mapThrowableToResultErrorUseCase(it) }
    }

    override fun apply(upstream: Observable<T>): ObservableSource<Result<T>> {
        return upstream
            .map<Result<T>> { Result.Success(it) }
            .onErrorReturn { mapThrowableToResultErrorUseCase(it) }
    }
}
