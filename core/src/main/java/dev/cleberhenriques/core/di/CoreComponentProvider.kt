package dev.cleberhenriques.core.di

interface CoreComponentProvider {
    val coreComponent: CoreComponent
}