package dev.cleberhenriques.core.di

import dagger.Component
import retrofit2.Retrofit

@Component(modules = [NetworkModule::class])
interface CoreComponent {

    @Component.Factory
    interface Factory {
        fun create(networkModule: NetworkModule): CoreComponent
    }

    fun provideRetrofit(): Retrofit
}
