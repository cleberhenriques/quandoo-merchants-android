package dev.cleberhenriques.core.ui

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.viewpager.widget.ViewPager
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception


@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
    Picasso.get()
        .load(url)
        .fit()
        .centerCrop()
        .into(imageView)

}
