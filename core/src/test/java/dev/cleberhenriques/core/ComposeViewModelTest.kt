package dev.cleberhenriques.core

import dev.cleberhenriques.core.domain.MapThrowableToResultErrorUseCase
import dev.cleberhenriques.core.domain.Result
import dev.cleberhenriques.core.transformations.ComposeViewModel
import io.reactivex.Observable
import io.reactivex.Single
import org.assertj.core.api.Assertions
import org.junit.Rule
import org.junit.Test

class ComposeViewModelTest {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun whenObservableStreamEmmitingWithSuccess_composeResultMustTransformStreamIntoResultSuccess() {
        val observable = Observable.just("1", "2", "3")
        val testObserver = observable.compose(ComposeViewModel()).test()
        Assertions.assertThat(testObserver.values()).isEqualTo(
            listOf(
                Result.Success("1"),
                Result.Success("2"),
                Result.Success("3")
            )
        )
    }

    @Test
    fun whenObservableStreamEmmitsError_composeResultMustTransformStreamIntoResultError() {
        val exception = Exception()
        val observable = Observable.error<Exception>(exception)
        val testObserver = observable.compose(ComposeViewModel()).test()
        Assertions.assertThat(testObserver.values()[0]).isEqualTo(
            MapThrowableToResultErrorUseCase.defaultError.copy(
                exception = exception
            )
        )
    }

    @Test
    fun whenSingleStreamEmmitingWithSuccess_composeResultMustTransformStreamIntoResultSuccess() {
        val single = Single.just("1")
        val testObserver = single.compose(ComposeViewModel()).test()
        Assertions.assertThat(testObserver.values()[0]).isEqualTo(
            Result.Success("1")
        )
    }

    @Test
    fun whenSingleStreamEmmitsError_composeResultMustTransformStreamIntoResultError() {
        val exception = Exception()
        val single = Single.error<Exception>(exception)
        val testObserver = single.compose(ComposeViewModel()).test()
        Assertions.assertThat(testObserver.values()[0]).isEqualTo(
            MapThrowableToResultErrorUseCase.defaultError.copy(
                exception = exception
            )
        )
    }
}
