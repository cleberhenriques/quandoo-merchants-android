package dev.cleberhenriques.core

import dev.cleberhenriques.core.domain.MapThrowableToResultErrorUseCase
import dev.cleberhenriques.core.domain.Result
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import retrofit2.HttpException
import java.io.InterruptedIOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class MapThroableToResultErrorUseCaseTest {

    val sut = MapThrowableToResultErrorUseCase()

    @Test
    fun whenThrowableIsSocketTimeOutException_mustReturnTimeOutError() {
        val exception = SocketTimeoutException()
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.timeOutError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsInterruptedIoException_mustReturnTimeOutError() {
        val exception = InterruptedIOException()
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.timeOutError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsUnknownHotException_mustReturnConnectivityError() {
        val exception = UnknownHostException()
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.connectivityError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsUnknown_mustReturnDefaultError() {
        val exception = Exception()
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withKnownErrorBody_mustReturnMappedResultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "{\"errorType\":\"error_type\",\"errorMessage\":\"error_message\"}"
        val error = sut(exception)
        assertThat(error).isEqualTo(Result.Error("error_type", "error_message", exception))
    }

    @Test
    fun whenThrowableIsHttpException_withEmptyErrorType_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "{\"errorType\":\"\",\"errorMessage\":\"error_message\"}"
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withEmptyErrorMessage_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "{\"errorType\":\"error_type\",\"errorMessage\":\"\"}"
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withNullErrorType_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "{\"errorType\":null,\"errorMessage\":\"error_message\"}"
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withNullErrorMessage_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "{\"errorType\":\"error_type\",\"errorMessage\":null}"
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withNullErrorBodyString_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns null
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withNullErrorBody_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()
        } returns null
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withNullResponse_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()
        } returns null
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }

    @Test
    fun whenThrowableIsHttpException_withUnknownErrorBody_mustReturnDefaultError() {
        val exception = mockk<HttpException>(relaxed = true)
        every {
            exception.response()?.errorBody()?.string()
        } returns "UNKNOWN ERROR BODY"
        val error = sut(exception)
        assertThat(error).isEqualTo(MapThrowableToResultErrorUseCase.defaultError.copy(exception = exception))
    }
}
