# Quandoo Merchants Android Test
![Image of app](https://media.giphy.com/media/f6aTMVJY1XnIIzV0zm/giphy.gif)

## About implementation
### Principal dependencies used
* Uses Dagger2 for Dependency Injection management
* Uses Retrofit for the API communication layer.
* Uses RxJava2 + RxKotlin to manage the data stream that comes from the data layer.
* Uses Android ArchComponents MVVM, LiveData & DataBinding for the presentation layer
* Uses Android Navigation + SafeArgs to handle navigation and fragment arguments type safe. 

### Modules
Although this project is small, I chose to develop it using a modularization approach to ensure perfect isolation between features. This improves reuse, cohesion and build time.

* The project have 3 modules: App, Core & Merchants.
    * Merchants is a feature module containing everything related to the list & detail of a Merchant.
    * Core module contains classes & dependency injection configs that will be used by all others modules of the app.
    * App module is the main application module, it contains all configs needed to generate the app APK, such as Application subclass, manifest & etc

![Modules images](modules.png)

### Tests

* Core module coverage: 80%
* Merchants module coverage: 43%
* App module doesn't have tests.
* It uses jacoco to generate Unit Tests reports
* Mockk was used to help mock objects
* AssertJ was used to turn tests more idiomatic.

Gradle tasks to generate reports:

    ./gradlew :core:jacocoTestDebugUnitTestReport

outputs report in `/core/build/reports/jacoco/jacocoTestDebugUnitTestReport/html/index.html`

    ./gradlew :merchants:jacocoTestDebugUnitTestReport

outputs report in `/merchants/build/reports/jacoco/jacocoTestDebugUnitTestReport/html/index.html`

### Improvements if i have more time:

* Improve test coverage by implementing ui tests.
* Implement an offline mode.
* Test module to share commons classes used in testing. eg: RxImmediateTestScheduler.kt
* Refactor to a better UI state handling approach.
* Integrate with online CI tool





    
