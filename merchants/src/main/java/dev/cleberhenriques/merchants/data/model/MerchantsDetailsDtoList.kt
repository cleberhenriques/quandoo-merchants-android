package dev.cleberhenriques.merchants.data.model

import com.google.gson.annotations.SerializedName

data class MerchantsDetailsDtoList(
    @SerializedName("merchants")
    val merchants: List<MerchantsDetailsDto>,
    @SerializedName("size")
    val size: Int,
    @SerializedName("offset")
    val offset: Int,
    @SerializedName("limit")
    val limit: Int
)
