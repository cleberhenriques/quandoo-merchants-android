package dev.cleberhenriques.merchants.data.mappers

import dev.cleberhenriques.merchants.data.model.TagGroupDto

fun List<TagGroupDto>.toCuisineTags(): List<String>? {
    return filter { it.type == "CUISINE" }.firstOrNull()?.tags?.map { it.name }
}