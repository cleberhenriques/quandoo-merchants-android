package dev.cleberhenriques.merchants.data.mappers

import dev.cleberhenriques.merchants.data.model.MerchantsDetailsDto
import dev.cleberhenriques.merchants.domain.model.Merchant

fun MerchantsDetailsDto.toMerchant(): Merchant {
    return Merchant(
        id,
        name,
        phoneNumber,
        reviewScore,
        images?.map { it.url },
        locationDto.toLocation(),
        tagGroupsDto?.toCuisineTags()
    )
}