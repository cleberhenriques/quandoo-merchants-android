package dev.cleberhenriques.merchants.data.api

import dev.cleberhenriques.merchants.data.model.MerchantsDetailsDtoList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MerchantsApi {
    @GET("/v1/merchants")
    fun getMerchants(@Query("offset") offset: Int, @Query("limit") limit: Int): Single<MerchantsDetailsDtoList>
}