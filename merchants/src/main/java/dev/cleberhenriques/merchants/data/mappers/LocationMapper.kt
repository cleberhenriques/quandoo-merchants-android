package dev.cleberhenriques.merchants.data.mappers

import dev.cleberhenriques.merchants.data.model.AddressDto
import dev.cleberhenriques.merchants.data.model.CoordinatesDto
import dev.cleberhenriques.merchants.data.model.LocationDto
import dev.cleberhenriques.merchants.domain.model.Address
import dev.cleberhenriques.merchants.domain.model.Coordinates
import dev.cleberhenriques.merchants.domain.model.Location


fun LocationDto.toLocation(): Location {
    return Location(coordinatesDto?.toCoordinates(), addressDto?.toAddress())
}

fun CoordinatesDto.toCoordinates(): Coordinates {
    return Coordinates(latitude, longitude)
}

fun AddressDto.toAddress(): Address {
    return Address(street, number, zipcode, city, country)
}