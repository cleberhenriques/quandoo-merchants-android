package dev.cleberhenriques.merchants.data

import dev.cleberhenriques.merchants.data.api.MerchantsApi
import dev.cleberhenriques.merchants.data.mappers.toMerchant
import dev.cleberhenriques.merchants.domain.model.Merchant
import io.reactivex.Single
import javax.inject.Inject

class MerchantsRepository @Inject constructor(private val merchantsApi: MerchantsApi) {

    fun getMerchants(offset: Int, limit: Int): Single<List<Merchant>> {
        return merchantsApi.getMerchants(offset, limit)
            .map { item -> item.merchants.map { it.toMerchant() } }
    }
}
