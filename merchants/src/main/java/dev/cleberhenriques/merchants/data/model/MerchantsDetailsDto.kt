package dev.cleberhenriques.merchants.data.model

import com.google.gson.annotations.SerializedName
import dev.cleberhenriques.merchants.domain.model.Merchant

data class MerchantsDetailsDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("reviewScore")
    val reviewScore: String,
    @SerializedName("images")
    val images: List<ImageDto>?,
    @SerializedName("location")
    val locationDto: LocationDto,
    @SerializedName("tagGroups")
    val tagGroupsDto: List<TagGroupDto>?
)

data class TagGroupDto(
    @SerializedName("type")
    val type: String,
    @SerializedName("tags")
    val tags: List<TranslatedTagDto>
)

data class TranslatedTagDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)

data class ImageDto(
    @SerializedName("url")
    val url: String
)

data class LocationDto(
    @SerializedName("coordinates")
    val coordinatesDto: CoordinatesDto?,
    @SerializedName("address")
    val addressDto: AddressDto?
)

data class AddressDto(
    @SerializedName("street")
    val street: String?,
    @SerializedName("number")
    val number: String?,
    @SerializedName("zipcode")
    val zipcode: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("country")
    val country: String?
)

data class CoordinatesDto(
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double
)


