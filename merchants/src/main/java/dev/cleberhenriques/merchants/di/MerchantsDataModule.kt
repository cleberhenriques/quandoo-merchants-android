package dev.cleberhenriques.merchants.di

import dagger.Module
import dagger.Provides
import dev.cleberhenriques.merchants.data.api.MerchantsApi
import retrofit2.Retrofit

@Module
class MerchantsDataModule {

    @Provides
    fun provideGetMerchantsApi(retrofit: Retrofit): MerchantsApi {
        return retrofit.create(MerchantsApi::class.java)
    }

}
