package dev.cleberhenriques.merchants.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dev.cleberhenriques.merchants.ui.list.MerchantsListFragment
import dev.cleberhenriques.merchants.ui.list.MerchantsListViewModel
import dev.cleberhenriques.merchants.ui.list.MerchantsListViewModelFactory

@Module
class MerchantsViewModelModule(private val fragment: MerchantsListFragment) {

    @Provides
    fun provideViewModel(factory: MerchantsListViewModelFactory): MerchantsListViewModel {
        return ViewModelProvider(fragment, factory).get(MerchantsListViewModel::class.java)
    }
}