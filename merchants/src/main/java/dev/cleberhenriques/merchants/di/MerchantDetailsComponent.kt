package dev.cleberhenriques.merchants.di

import dagger.Component
import dev.cleberhenriques.core.di.CoreComponent
import dev.cleberhenriques.merchants.ui.details.MerchantDetailFragment

@Component(
    modules = [
        MerchantsDataModule::class,
        MerchantDetailViewModelModule::class
    ],
    dependencies = [
        CoreComponent::class
    ]
)
interface MerchantDetailsComponent {

    @Component.Factory
    interface Factory {
        fun create(
            coreComponent: CoreComponent,
            merchantDetailViewModelModule: MerchantDetailViewModelModule
        ): MerchantDetailsComponent
    }

    fun inject(merchantsDetailFragment: MerchantDetailFragment)
}
