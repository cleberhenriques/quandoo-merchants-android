package dev.cleberhenriques.merchants.di

import dagger.Component
import dev.cleberhenriques.core.di.CoreComponent
import dev.cleberhenriques.merchants.ui.list.MerchantsListFragment

@Component(
    modules = [
        MerchantsDataModule::class,
        MerchantsViewModelModule::class
    ],
    dependencies = [
        CoreComponent::class
    ]
)
interface MerchantsListComponent {

    @Component.Factory
    interface Factory {
        fun create(
            coreComponent: CoreComponent,
            merchantsListModule: MerchantsViewModelModule
        ): MerchantsListComponent
    }

    fun inject(merchantsListFragment: MerchantsListFragment)
}
