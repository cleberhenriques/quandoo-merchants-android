package dev.cleberhenriques.merchants.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.ui.details.MerchantDetailFragment
import dev.cleberhenriques.merchants.ui.details.MerchantDetailViewModel
import dev.cleberhenriques.merchants.ui.details.MerchantDetailViewModelFactory

@Module
class MerchantDetailViewModelModule(
    private val fragment: MerchantDetailFragment,
    private val merchant: Merchant
) {

    @Provides
    fun provideViewModel(factory: MerchantDetailViewModelFactory): MerchantDetailViewModel {
        return ViewModelProvider(fragment, factory).get(MerchantDetailViewModel::class.java)
    }

    @Provides
    fun provideViewModelFactory(): MerchantDetailViewModelFactory {
        return MerchantDetailViewModelFactory(merchant)
    }
}