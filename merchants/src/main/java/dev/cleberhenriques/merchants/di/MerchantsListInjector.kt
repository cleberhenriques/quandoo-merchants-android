package dev.cleberhenriques.merchants.di

import dev.cleberhenriques.core.di.CoreComponentProvider
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.ui.details.MerchantDetailFragment
import dev.cleberhenriques.merchants.ui.list.MerchantsListFragment

fun MerchantsListFragment.inject() {

    DaggerMerchantsListComponent.factory()
        .create(
            (requireActivity().application as CoreComponentProvider).coreComponent,
            MerchantsViewModelModule(this)
        )
        .inject(this)
}

fun MerchantDetailFragment.inject(merchant: Merchant) {

    DaggerMerchantDetailsComponent.factory()
        .create(
            (requireActivity().application as CoreComponentProvider).coreComponent,
            MerchantDetailViewModelModule(this, merchant)
        )
        .inject(this)
}