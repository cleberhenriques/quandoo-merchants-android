package dev.cleberhenriques.merchants.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(
    val street: String?,
    val number: String?,
    val zipcode: String?,
    val city: String?,
    val country: String?
) : Parcelable