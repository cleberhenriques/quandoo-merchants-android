package dev.cleberhenriques.merchants.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
    val coordinates: Coordinates?,
    val address: Address?
): Parcelable
