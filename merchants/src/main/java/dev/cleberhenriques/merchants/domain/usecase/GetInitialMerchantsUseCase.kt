package dev.cleberhenriques.merchants.domain.usecase

import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.domain.model.Merchant
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class GetInitialMerchantsUseCase @Inject constructor(private val repository: MerchantsRepository) {

    private val pageLimit = 30

    operator fun invoke(numberOfItems: Int = 120): Observable<List<Merchant>> {
        return Observable.fromIterable(getMerchants(numberOfItems))
            .concatMap { it.toObservable() }
    }

    private fun getMerchants(numberOfItems: Int): List<Single<List<Merchant>>> {

        val calls = mutableListOf<Single<List<Merchant>>>()
        var offset = 0
        while (offset < numberOfItems) {

            val remainingItems = numberOfItems - offset
            if (remainingItems < pageLimit) {
                calls.add(repository.getMerchants(offset, remainingItems))
                break
            }

            calls.add(repository.getMerchants(offset, pageLimit))
            offset += pageLimit
        }

        return calls
    }

}