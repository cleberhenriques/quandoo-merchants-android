package dev.cleberhenriques.merchants.domain.usecase

import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.domain.model.Merchant
import io.reactivex.Single
import javax.inject.Inject

class GetMerchantsUseCase @Inject constructor(private val repository: MerchantsRepository) {

    private val limit = 30

    operator fun invoke(offset: Int): Single<List<Merchant>> {
        return repository.getMerchants(offset, limit)
    }
}