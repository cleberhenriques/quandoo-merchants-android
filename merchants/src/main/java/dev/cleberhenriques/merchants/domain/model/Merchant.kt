package dev.cleberhenriques.merchants.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Merchant(
    val id: Int,
    val name: String,
    val phoneNumber: String,
    val reviewScore: String,
    val images: List<String>?,
    val location: Location? = null,
    val cuisineTags: List<String>? = null
) : Parcelable
