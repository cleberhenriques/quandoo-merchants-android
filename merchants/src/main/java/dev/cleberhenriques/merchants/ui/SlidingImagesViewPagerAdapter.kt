package dev.cleberhenriques.merchants.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import dev.cleberhenriques.merchants.R
import dev.cleberhenriques.merchants.databinding.SlidingImageLayoutBinding

class SlidingImagesViewPagerAdapter(context: Context, private val images: List<String>) :
    PagerAdapter() {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = DataBindingUtil.inflate<SlidingImageLayoutBinding>(
            inflater,
            R.layout.sliding_image_layout,
            container,
            false
        )
        view.imageUrl = images[position]
        container.addView(view.root)
        return view.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return images.size
    }

}


@BindingAdapter("images")
fun loadImages(viewPager: ViewPager, images: List<String>?) {
    images?.let {
        viewPager.adapter =
            SlidingImagesViewPagerAdapter(
                viewPager.context.applicationContext,
                it
            )
    }
}