package dev.cleberhenriques.merchants.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.cleberhenriques.merchants.R
import dev.cleberhenriques.merchants.domain.model.Merchant
import javax.inject.Inject

class MerchantDetailViewModel @Inject constructor(private val merchant: Merchant) : ViewModel() {

    val uiModelEvent = MutableLiveData<MerchantDetailUiModel>()
    val phoneEvent = MutableLiveData<String>()

    init {
        uiModelEvent.value = getMerchantUiModel()
    }

    fun onCallRestaurantClick() {
        phoneEvent.value = merchant.phoneNumber
    }

    private fun getMerchantUiModel(): MerchantDetailUiModel {
        val images = merchant.images
        val name = merchant.name
        val address = merchant.location?.address
        val addressFirstLine = listOfNotNull(address?.street, address?.number).joinToString(" ")
        val addressSecondLine = listOfNotNull(address?.zipcode, address?.city).joinToString(", ")
        val cuisine = merchant.cuisineTags?.firstOrNull() ?: "Unknown cuisine"
        val score = "${merchant.reviewScore}/6"
        val scoreColor = merchant.reviewScore.toDoubleOrNull()?.let {
           when {
                it <= 2 -> {
                    R.color.score_slow
                }
                it < 4 -> {
                    R.color.score_medium
                }
                else -> {
                    R.color.score_high
                }
            }
        } ?: R.color.score_medium

        return MerchantDetailUiModel(images ?: emptyList(), name, addressFirstLine, addressSecondLine, score, scoreColor, cuisine)
    }
}
