package dev.cleberhenriques.merchants.ui.details

data class MerchantDetailUiModel(
    val images: List<String>,
    val name: String,
    val addressFirstLine: String,
    val addressSecondLine: String,
    val score: String,
    val scoreColor: Int,
    val cuisine: String
)