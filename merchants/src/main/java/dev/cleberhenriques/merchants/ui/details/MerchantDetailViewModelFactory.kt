package dev.cleberhenriques.merchants.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dev.cleberhenriques.merchants.domain.model.Merchant

@Suppress("UNCHECKED_CAST")
class MerchantDetailViewModelFactory constructor(
    val merchant: Merchant
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MerchantDetailViewModel(merchant) as T
    }

}
