package dev.cleberhenriques.merchants.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import dev.cleberhenriques.merchants.R
import dev.cleberhenriques.merchants.databinding.MerchantListItemBinding

class MerchantsAdapter(private val onItemSelected: (index: Int) -> Unit):
    RecyclerView.Adapter<MerchantsAdapter.MerchantsViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MerchantsViewHolder {
        val merchantsListBinding: MerchantListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup.context),
            R.layout.merchant_list_item, viewGroup, false
        )
        return MerchantsViewHolder(merchantsListBinding)
    }

    override fun onBindViewHolder(merchantsViewHolder: MerchantsViewHolder, i: Int) {
        merchantsViewHolder.merchantsListItemBinding.item = differ.currentList[i]
        merchantsViewHolder.merchantsListItemBinding.root.setOnClickListener {
            onItemSelected(i)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun setMerchants(merchants: List<MerchantUiModel>) {
        differ.submitList(merchants)
    }

    class MerchantsViewHolder(val merchantsListItemBinding: MerchantListItemBinding) :
        RecyclerView.ViewHolder(merchantsListItemBinding.root)

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MerchantUiModel>() {
        override fun areItemsTheSame(oldItem: MerchantUiModel, newItem: MerchantUiModel): Boolean {
            return oldItem.imageUrl == newItem.imageUrl
        }

        override fun areContentsTheSame(
            oldItem: MerchantUiModel,
            newItem: MerchantUiModel
        ): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)
}




