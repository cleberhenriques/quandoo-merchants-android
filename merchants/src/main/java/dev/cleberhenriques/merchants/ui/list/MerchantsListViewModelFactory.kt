package dev.cleberhenriques.merchants.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.domain.usecase.GetInitialMerchantsUseCase
import dev.cleberhenriques.merchants.domain.usecase.GetMerchantsUseCase
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class MerchantsListViewModelFactory @Inject constructor(
    val initialMerchantsUseCase: GetInitialMerchantsUseCase,
    val getMerchantsUseCase: GetMerchantsUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MerchantsListViewModel(initialMerchantsUseCase, getMerchantsUseCase) as T
    }

}
