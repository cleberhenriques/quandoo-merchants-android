package dev.cleberhenriques.merchants.ui.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dev.cleberhenriques.merchants.R
import dev.cleberhenriques.merchants.databinding.FragmentMerchantsListBinding
import dev.cleberhenriques.merchants.di.inject
import javax.inject.Inject

class MerchantsListFragment : Fragment() {

    @Inject
    lateinit var merchantsListViewModel: MerchantsListViewModel
    lateinit var binding: FragmentMerchantsListBinding
    private lateinit var adapter: MerchantsAdapter
    private val thresholdToLoadMoreItems = 10
    private val numberOfColumns = 3

    var loadingSnackbar: Snackbar? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        inject()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_merchants_list, container, false)

        configureRecycler()
        observeViewModel()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingSnackbar = Snackbar.make(binding.root, getString(R.string.loading), Snackbar.LENGTH_INDEFINITE)
    }

    private fun configureRecycler() {
        adapter = MerchantsAdapter(::onItemSelected)
        binding.merchantsList.adapter = adapter
        binding.merchantsList.layoutManager = GridLayoutManager(requireContext(), numberOfColumns)
        binding.merchantsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
                if (totalItemCount <= lastVisibleItemPosition + thresholdToLoadMoreItems) {
                    merchantsListViewModel.loadMore()
                }
            }
        })
    }

    fun onItemSelected(index: Int) {
        findNavController().navigate(
            MerchantsListFragmentDirections.actionMerchantsListFragmentToMerchantDetailFragment(
                merchantsListViewModel.getMerchantAt(index)
            )
        )
    }

    private fun observeViewModel() {
        merchantsListViewModel.merchantUiModelListEvent.observe(viewLifecycleOwner, Observer {
            adapter.setMerchants(it)
        })

        merchantsListViewModel.errorEvent.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show()
        })

        merchantsListViewModel.loadingStatusEvent.observe(viewLifecycleOwner, Observer {
            if (it) {
                loadingSnackbar?.show()
            } else {
                loadingSnackbar?.dismiss()
            }
        })
    }
}
