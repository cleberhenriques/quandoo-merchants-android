package dev.cleberhenriques.merchants.ui.details


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import dev.cleberhenriques.merchants.R
import dev.cleberhenriques.merchants.databinding.FragmentMerchantDetailBinding
import dev.cleberhenriques.merchants.di.inject
import javax.inject.Inject

class MerchantDetailFragment : Fragment() {

    @Inject
    lateinit var viewModel: MerchantDetailViewModel
    lateinit var binding: FragmentMerchantDetailBinding
    private val args: MerchantDetailFragmentArgs by navArgs()

    override fun onAttach(context: Context) {
        inject(args.selectedMerchant)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_merchant_detail, container, false)
        binding.viewModel = viewModel
        observeViewModel()
        return binding.root
    }

    fun observeViewModel() {
        viewModel.phoneEvent.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), getString(R.string.calling_number, it), Toast.LENGTH_LONG).show()
        })
    }

}
