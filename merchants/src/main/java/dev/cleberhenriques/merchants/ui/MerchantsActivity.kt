package dev.cleberhenriques.merchants.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dev.cleberhenriques.merchants.R

class MerchantsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchants)
    }
}
