package dev.cleberhenriques.merchants.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.cleberhenriques.core.domain.Result
import dev.cleberhenriques.core.transformations.ComposeViewModel
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.domain.usecase.GetInitialMerchantsUseCase
import dev.cleberhenriques.merchants.domain.usecase.GetMerchantsUseCase
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class MerchantsListViewModel constructor(
    getInitialMerchantsUseCase: GetInitialMerchantsUseCase,
    private val getMerchantsUseCase: GetMerchantsUseCase
) :
    ViewModel() {

    private val disposable = CompositeDisposable()
    private val merchants = mutableListOf<Merchant>()
    private val initialLoadSize = 120

    val merchantUiModelListEvent = MutableLiveData<List<MerchantUiModel>>()
    val errorEvent = MutableLiveData<String>()
    val loadingStatusEvent = MutableLiveData<Boolean>(false)

    init {
        getInitialMerchantsUseCase(initialLoadSize)
            .compose(ComposeViewModel())
            .doOnSubscribe { loadingStatusEvent.value = true }
            .doFinally { loadingStatusEvent.value = false }
            .subscribe(::onResult)
            .addTo(disposable)
    }

    fun loadMore() {
        if (loadingStatusEvent.value != true) {
            getMerchantsUseCase(merchants.size)
                .compose(ComposeViewModel())
                .doOnSubscribe { loadingStatusEvent.value = true }
                .doFinally { loadingStatusEvent.value = false }
                .subscribe(::onResult)
                .addTo(disposable)
        }
    }

    private fun onResult(result: Result<List<Merchant>>) {
        when (result) {
            is Result.Success -> onMerchantsReceived(result.data)
            is Result.Error -> errorEvent.value = result.message
        }
    }

    private fun onMerchantsReceived(items: List<Merchant>) {
        merchants.addAll(items)
        merchantUiModelListEvent.value =
            merchants.map { MerchantUiModel(it.name, it.images?.firstOrNull()) }
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }

    fun getMerchantAt(index: Int): Merchant {
        return merchants[index]
    }
}

data class MerchantUiModel(
    val title: String,
    val imageUrl: String?
)
