package dev.cleberhenriques.merchants

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import dev.cleberhenriques.merchants.domain.model.Address
import dev.cleberhenriques.merchants.domain.model.Location
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.ui.details.MerchantDetailUiModel
import dev.cleberhenriques.merchants.ui.details.MerchantDetailViewModel
import io.mockk.mockk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MerchantDetailViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val merchantStubHighScore = Merchant(
        123,
        "Restaurant 1",
        "12345678",
        "5.8",
        listOf("url1", "url2"),
        Location(null, Address("Street", "number", "zipcode", "city", "country")),
        listOf("tag 1", "tag 2")
    )

    private val merchantStubSlowScore = merchantStubHighScore.copy(reviewScore = "1.8")
    private val merchantStubAverageScore = merchantStubHighScore.copy(reviewScore = "3.0")
    private val merchantStubNoCuisine = merchantStubAverageScore.copy(cuisineTags = null)
    private val merchantStubInvalidScoreWithoutStreet = merchantStubNoCuisine.copy(
        reviewScore = "3.0/4",
        location = Location(null, Address(null, "number", "zipcode", "city", "country"))
    )
    private val merchantStubWithoutNumber = merchantStubInvalidScoreWithoutStreet.copy(
        location = Location(
            null,
            Address("Street", null, "zipcode", "city", "country")
        )
    )

    @Test
    fun whenInitWithMerchant_mustEmmitUiModel() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubHighScore)
        sut.uiModelEvent.observeForever(testObserver)
        verify { testObserver.onChanged(any()) }
    }

    @Test
    fun whenInitWithMerchant_mustMapToMerchantDetailUiModel() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubHighScore)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "Street number",
            "zipcode, city",
            "5.8/6",
            R.color.score_high,
            "tag 1"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenInitWithMerchantWithSlowScore_mustMapToMerchantDetailUiModelWithScoreSlowColor() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubSlowScore)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "Street number",
            "zipcode, city",
            "1.8/6",
            R.color.score_slow,
            "tag 1"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenInitWithMerchantWithAverageScore_mustMapToMerchantDetailUiModelWithScoreMediumColor() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubAverageScore)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "Street number",
            "zipcode, city",
            "3.0/6",
            R.color.score_medium,
            "tag 1"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenInitWithMerchantWithNoCuisineTag_mustMapToMerchantDetailUiModelWithUnknownCuisine() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubNoCuisine)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "Street number",
            "zipcode, city",
            "3.0/6",
            R.color.score_medium,
            "Unknown cuisine"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenInitWithMerchantWithInvalidScore_mustMapToMerchantDetailUiModelWithDefaultColor() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubInvalidScoreWithoutStreet)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "number",
            "zipcode, city",
            "3.0/4/6",
            R.color.score_medium,
            "Unknown cuisine"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenInitWithMerchantWithEmptyNumber_mustMapToMerchantDetailUiModelWithStreetOnly() {
        val testObserver = mockk<Observer<MerchantDetailUiModel>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubWithoutNumber)
        sut.uiModelEvent.observeForever(testObserver)
        val correctUiModel = MerchantDetailUiModel(
            listOf("url1", "url2"),
            "Restaurant 1",
            "Street",
            "zipcode, city",
            "3.0/4/6",
            R.color.score_medium,
            "Unknown cuisine"
        )
        verify { testObserver.onChanged(correctUiModel) }
    }

    @Test
    fun whenCallRestaurantClick_mustEmmitPhoneEvent() {
        val testObserver = mockk<Observer<String>>(relaxed = true)
        val sut = MerchantDetailViewModel(merchantStubWithoutNumber)
        sut.onCallRestaurantClick()
        sut.phoneEvent.observeForever(testObserver)
        verify { testObserver.onChanged("12345678") }
    }

}
