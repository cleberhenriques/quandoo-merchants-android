package dev.cleberhenriques.merchants

import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.domain.usecase.GetMerchantsUseCase
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class GetMerchantsUseCaseTest {

    private val merchantsRepository = mockk<MerchantsRepository>(relaxed = true)
    private val getMerchantsUseCase = GetMerchantsUseCase(merchantsRepository)

    @Test
    fun getMerchants_shouldRequestPageWith30ItemsLimit() {
        getMerchantsUseCase(0)
        verify { merchantsRepository.getMerchants(0, 30) }
        getMerchantsUseCase(30)
        verify { merchantsRepository.getMerchants(30, 30) }
    }
}