package dev.cleberhenriques.merchants

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.domain.usecase.GetInitialMerchantsUseCase
import dev.cleberhenriques.merchants.domain.usecase.GetMerchantsUseCase
import dev.cleberhenriques.merchants.ui.list.MerchantUiModel
import dev.cleberhenriques.merchants.ui.list.MerchantsListViewModel
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.util.concurrent.TimeUnit


class MerchantsListViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    var immediateSchedulerRule = RxImmediateSchedulerRule()

    private val testScheduler = TestScheduler()
    private val getInitialMerchantsUseCase = mockk<GetInitialMerchantsUseCase>(relaxed = true)
    private val getMerchantsUseCase = mockk<GetMerchantsUseCase>(relaxed = true)

    @Test
    fun onViewModelInit_mustRequestFirst120Merchants() {
        MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        verify { getInitialMerchantsUseCase(120) }
    }

    @Test
    fun onMerchantsReceived_mustEmitUpdatedListOfMerchantsUiModel() {
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(getMerchantsStub())

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<List<MerchantUiModel>>>(relaxed = true)

        sut.merchantUiModelListEvent.observeForever(testObserver)

        verify { testObserver.onChanged(getMerchantUiModelStub()) }
    }

    @Test
    fun onErrorReceived_mustEmitErrorEvent() {
        every { getInitialMerchantsUseCase(any()) } returns Observable.error(Exception())

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<String>>(relaxed = true)

        sut.errorEvent.observeForever(testObserver)

        verify { testObserver.onChanged("Sorry, something went wrong") }
    }

    @Test
    fun whenRequestingFirstMerchants_mustChangeLoadingStatusToTrue() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(getMerchantsStub())
            .delayEach(100, TimeUnit.MILLISECONDS)

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<Boolean>>(relaxed = true)
        sut.loadingStatusEvent.observeForever(testObserver)

        verify { testObserver.onChanged(true) }
        verify(exactly = 0) { testObserver.onChanged(false) }
    }

    @Test
    fun whenFinishRequestingFirstMerchants_mustChangeLoadingStatusToFalse() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        val items = getMerchantsStub()
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(items)
            .delayEach(100, TimeUnit.MILLISECONDS)

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<Boolean>>(relaxed = true)
        sut.loadingStatusEvent.observeForever(testObserver)

        testScheduler.advanceTimeBy(items.size * 100L, TimeUnit.MILLISECONDS)
        verify(exactly = 1) { testObserver.onChanged(false) }
    }

    @Test
    fun whenLoadMoreIsCalled_andLoadingStatusIsTrue_mustNotLoadMoreItems() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        val items = getMerchantsStub()
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(items)
            .delayEach(100, TimeUnit.MILLISECONDS)

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<Boolean>>(relaxed = true)
        sut.loadingStatusEvent.observeForever(testObserver)

        assertThat(sut.loadingStatusEvent.value).isEqualTo(true)
        sut.loadMore()
        verify(exactly = 0) { getMerchantsUseCase(any()) }
    }

    @Test
    fun whenLoadMoreIsCalled_andLoadingStatusIsFalse_mustRequestMoreItems() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        val items = getMerchantsStub()
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(items)
            .delayEach(100, TimeUnit.MILLISECONDS)
        every { getMerchantsUseCase(any()) } returns Observable.fromIterable(items).singleOrError()

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        val testObserver = mockk<Observer<Boolean>>(relaxed = true)
        sut.loadingStatusEvent.observeForever(testObserver)

        testScheduler.advanceTimeBy(items.size * 100L, TimeUnit.MILLISECONDS)
        assertThat(sut.loadingStatusEvent.value).isEqualTo(false)
        sut.loadMore()
        verify(exactly = 1) { getMerchantsUseCase(items.size) }
    }

    @Test
    fun whenLoadMoreReceivesError_mustEmmitErrorEvent() {
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        val items = getMerchantsStub()
        every { getInitialMerchantsUseCase(any()) } returns Observable.fromIterable(items)
        every { getMerchantsUseCase(any()) } returns Single.error(Exception())

        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)
        testScheduler.triggerActions()

        val testObserver = mockk<Observer<String>>(relaxed = true)
        sut.errorEvent.observeForever(testObserver)
        sut.loadMore()
        testScheduler.triggerActions()
        verify { testObserver.onChanged("Sorry, something went wrong") }
    }

    @Test
    fun onCleared_mustDispose() {
        val sut = MerchantsListViewModel(getInitialMerchantsUseCase, getMerchantsUseCase)

    }

    private fun getMerchantsStub(): List<List<Merchant>> {
        return listOf(
            listOf(Merchant(30, "Restaurant 30", "12345678", "10", listOf("url"))),
            listOf(Merchant(60, "Restaurant 60", "12345678", "10", listOf("url"))),
            listOf(Merchant(90, "Restaurant 90", "12345678", "10", listOf("url"))),
            listOf(Merchant(120, "Restaurant 120", "12345678", "10", listOf("url")))
        )
    }

    private fun getMerchantUiModelStub(): List<MerchantUiModel> {
        return listOf(
            MerchantUiModel("Restaurant 30", "url"),
            MerchantUiModel("Restaurant 60", "url"),
            MerchantUiModel("Restaurant 90", "url"),
            MerchantUiModel("Restaurant 120", "url")
        )
    }
}


private fun <T> Observable<T>.delayEach(interval: Long, timeUnit: TimeUnit): Observable<T> =
    Observable.zip(
        this,
        Observable.interval(interval, timeUnit),
        BiFunction { item, _ -> item }
    )