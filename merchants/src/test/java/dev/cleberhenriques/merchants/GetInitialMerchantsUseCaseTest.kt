package dev.cleberhenriques.merchants

import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.domain.model.Merchant
import dev.cleberhenriques.merchants.domain.usecase.GetInitialMerchantsUseCase
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifySequence
import io.reactivex.Single
import org.junit.Test

class GetInitialMerchantsUseCaseTest {

    private val merchantsRepository = mockk<MerchantsRepository>()
    private val sut = GetInitialMerchantsUseCase(merchantsRepository)

    @Test
    fun getInitial120Merchants_shouldRequest4PagesWith30Items() {
        mockGetInitialMerchants()

        sut().test()

        verifySequence {
            merchantsRepository.getMerchants(0, 30)
            merchantsRepository.getMerchants(30, 30)
            merchantsRepository.getMerchants(60, 30)
            merchantsRepository.getMerchants(90, 30)
        }
    }

    @Test
    fun getInitial110Merchants_shouldRequest3PagesWith30Items_and1PageWith20Items() {
        mockGetInitialMerchants()

        sut(110).test()

        verifySequence {
            merchantsRepository.getMerchants(0, 30)
            merchantsRepository.getMerchants(30, 30)
            merchantsRepository.getMerchants(60, 30)
            merchantsRepository.getMerchants(90, 20)
        }
    }


    private fun mockGetInitialMerchants() {
        every { merchantsRepository.getMerchants(any(), any()) } returns Single.just(
            listOf(
                Merchant(1, "Restaurant 1", "1234", "1", listOf("url")),
                Merchant(2, "Restaurant 2", "1234", "1", listOf("url")),
                Merchant(3, "Restaurant 3", "1234", "1", listOf("url")),
                Merchant(4, "Restaurant 4", "1234", "1", listOf("url"))
            )
        )
    }
}