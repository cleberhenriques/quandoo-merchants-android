package dev.cleberhenriques.merchants

import dev.cleberhenriques.merchants.data.MerchantsRepository
import dev.cleberhenriques.merchants.data.api.MerchantsApi
import dev.cleberhenriques.merchants.data.model.ImageDto
import dev.cleberhenriques.merchants.data.model.LocationDto
import dev.cleberhenriques.merchants.data.model.MerchantsDetailsDto
import dev.cleberhenriques.merchants.data.model.MerchantsDetailsDtoList
import dev.cleberhenriques.merchants.domain.model.Location
import dev.cleberhenriques.merchants.domain.model.Merchant
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class MerchantsRepositoryTest {

    private val merchantsApi = mockk<MerchantsApi>()
    private val sut = MerchantsRepository(merchantsApi)

    @Before
    fun setup() {

        val stubItems = listOf(
            MerchantsDetailsDto(
                123, "Restaurant 1", "12345678", "10", listOf(ImageDto("url")), LocationDto(null, null), null
            ),
            MerchantsDetailsDto(
                124, "Restaurant 2", "12345678", "10", listOf(ImageDto("url1"), ImageDto("url2")), LocationDto(null, null), null
            )
        )

        every { merchantsApi.getMerchants(0, 30) } returns Single.just(
            MerchantsDetailsDtoList(stubItems, stubItems.size, 0, 30)
        )
    }

    @Test
    fun getMerchants_shouldReturnMappedItems() {
        val observer = sut.getMerchants(0, 30).test()
        observer.assertResult(
            listOf(
                Merchant(
                    123, "Restaurant 1", "12345678", "10", listOf("url"), Location(null, null), null
                ),
                Merchant(
                    124, "Restaurant 2", "12345678", "10", listOf("url1", "url2"), Location(null, null), null
                )
            )
        )
    }

}